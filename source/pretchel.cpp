
#include "./../header/pretchel.h"




// Activation function

	// Tanh
	VectorXd tanh(VectorXd _input) {
		return VectorXd(Eigen::tanh(_input.array()));
	}
	VectorXd tanh_derive(VectorXd _input) {
		VectorXd tmp = (_input.Ones(_input.size()) - tanh(_input).cwiseProduct(tanh(_input)));
		return tmp;
	}

	// ReLU
	VectorXd ReLU(VectorXd _input) {
		VectorXd ret = _input;
		for (int i = 0; i < ret.size(); i++)
			if (ret[i] < 0)  ret[i] = 0;
		return ret;
	}
	VectorXd ReLU_derive(VectorXd _input) {
		VectorXd ret = VectorXd(_input.size());
		for (int i = 0; i < ret.size(); i++) {
			if (_input[i] < 0)  ret[i] = 0;
			else ret[i] = 1;
		}
		return ret;
	}

	//Softmax
	VectorXd softmaxLayer(VectorXd _input) {
		VectorXd expTmp;
		expTmp = exp(_input.array());
		double sum = expTmp.sum();
		return expTmp / sum;
	}

	VectorXd softmax_derive(VectorXd _input) {
		VectorXd expTmp;
		expTmp = exp(_input.array());
		double sum = expTmp.sum();
		return expTmp / sum;
	}


// Loss function (Objective function)
	VectorXd MSE(VectorXd _target, VectorXd _output) {
		return (_target - _output).cwiseAbs2() / 2;
	}

	VectorXd MSE_derive(VectorXd _target, VectorXd _output) {
		return _output - _target;
	}





// class ActivationLayer implementation

ActivationLayer::ActivationLayer() {

}

ActivationLayer::ActivationLayer(VectorXd(*_activation)(VectorXd), VectorXd(*_derive)(VectorXd), int _node) {
	activation = _activation;
	derive = _derive;
	o = VectorXd::Zero(_node);
}

void ActivationLayer::setFunction(VectorXd(*_activation)(VectorXd), VectorXd(*_derive)(VectorXd), int _node) {
	activation = _activation;
	derive = _derive;
	o = VectorXd::Zero(_node);
}
void ActivationLayer::feedForward(VectorXd _input) {
	o = activation(_input);
}
VectorXd ActivationLayer::calcDerive(VectorXd _input) {
	return derive(_input);
}

VectorXd ActivationLayer::getOutput()
{
	return this->o;
}


// class Layer implementation

Layer::Layer() {

}
Layer::Layer(int _node, int _inputCount, double _learningRate) {
	nodeCount = _node;
	learningRate = _learningRate;
	net = VectorXd::Zero(_node);
	delta = VectorXd::Zero(_node);
	w = MatrixXd::Zero(_inputCount, _node);
	b = VectorXd::Zero(_node);
}

// Load from JSON file
// json => vector => vector.data => VectorXf or MatrixXf
Layer::Layer(int _node, json _jsonFile) {
	nodeCount = _node;
	net = VectorXd::Zero(_node);
	//delta = MatrixXf::Zero(_inputLayer->nodeCount, _node);
	//w = MatrixXf::Zero(_inputLayer->nodeCount, _node);
	//inputLayer = _inputLayer;
}


int Layer::getNodeCount()
{
	return this->nodeCount;
}

void Layer::initLayer() {
	w.setRandom();
	w /= 5;
	b.setRandom();
}

void Layer::setActivation(VectorXd(*_activation)(VectorXd), VectorXd(*_derive)(VectorXd)) {
	this->actLayer = new ActivationLayer();
	this->actLayer->setFunction(_activation, _derive, this->nodeCount);
}

void Layer::feedForward(VectorXd _input) {
	//inputLayer�� ������ ������, �� ���� �̿��� ����Ѵ�.
	// ������� ���̾� ���ο� �����Ѵ�.
	this->net = w.transpose() * _input + b;
	if (this->actLayer != NULL) {
		this->actLayer->feedForward(this->net);
	}
}

void Layer::feedForward(VectorXd _input, VectorXd _output) {
	//inputLayer�� ������ ������, �� ���� �̿��� ����Ѵ�.
	// ������� ���̾� ���ο� �����Ѵ�.
	this->net = w.transpose() * _input + b;
	if (this->actLayer != NULL) {
		this->actLayer->feedForward(this->net);
		_output = this->actLayer->getOutput();
	}
	else {
		_output = this->net;
	}

}

void Layer::backpropagation(VectorXd _input, Layer* outputLayer) {
	// delta = W * pre_delta * f' 
	this->delta = this->actLayer->calcDerive(this->net).array() * (outputLayer->w * outputLayer->delta).array();
	w -= learningRate * _input * this->delta.transpose();
}

void Layer::calcLoss(VectorXd _input, MatrixXd _delta) {
	this->delta = _delta;
	w -= learningRate * _input * this->delta.transpose();
}

VectorXd Layer::getOutput() {
	if (this->actLayer != NULL) {
		return this->actLayer->getOutput();
	}
	else {
		return this->net;
	}
}

VectorXd Layer::getDerive() {
	return this->actLayer->calcDerive(this->net);
}

////Neural Network updater

Trainer::Trainer(vector<int> _hiddenNodes,
	double _learningRate,
	VectorXd(*_activation)(VectorXd),
	VectorXd(*_derive)(VectorXd)
) {
	hidden_layers.push_back(Layer(_hiddenNodes[1], _hiddenNodes[0], _learningRate));
	hidden_layers[0].setActivation(_activation, _derive);
	hidden_layers[0].initLayer();
	vector<int>::iterator it = _hiddenNodes.begin();
	it++;
	it++;
	for (it; it < _hiddenNodes.end(); it++) {
		hidden_layers.push_back(Layer(*it, *(it - 1), _learningRate));
		hidden_layers.back().setActivation(_activation, _derive);
		hidden_layers.back().initLayer();
	}
}

Trainer::Trainer(vector<int> _hiddenNodes,
	double _learningRate,
	VectorXd(*_activation)(VectorXd),
	VectorXd(*_derive)(VectorXd),
	VectorXd(*_outputActivation)(VectorXd),
	VectorXd(*_outputDerive)(VectorXd)
	) {
	hidden_layers.push_back(Layer(_hiddenNodes[1], _hiddenNodes[0], _learningRate));
	hidden_layers[0].setActivation(_activation, _derive);
	hidden_layers[0].initLayer();
	vector<int>::iterator it = _hiddenNodes.begin();
	it++;
	it++;
	for (it; it < _hiddenNodes.end() - 1; it++) {
		hidden_layers.push_back(Layer(*it, *(it-1), _learningRate));
		hidden_layers.back().setActivation(_activation, _derive);
		hidden_layers.back().initLayer();
	}
	//output layer
	hidden_layers.push_back(Layer(*it, *(it - 1), _learningRate));
	hidden_layers.back().setActivation(_outputActivation, _outputDerive);
	hidden_layers.back().initLayer();
}

void Trainer::setInputdata(vector<string>& _categories, string _trainRoute, string _testRoute) {
	// Read JSON data at '_fileRoute'
	int idx = 0;
	for (const string& category : _categories) {
		this->categories[category] = idx;
		idx++;
	}
	this->setTrainData(_trainRoute);
	this->setTestData(_testRoute);
}

void Trainer::setTrainData(string _trainRoute) {
	std::ifstream tr(_trainRoute);
	tr >> this->train_json;
	this->jsonToMat(this->train_json, this->train_mat);
}

void Trainer::setTestData(string _testRoute) {
	std::ifstream te(_testRoute);
	te >> this->test_json;
	this->jsonToMat(this->test_json, this->test_mat);
}

void Trainer::jsonToMat(json _j, MatrixXd& _m) {
	int r = _j.size();
	int c = _j[0].size();
	_m = MatrixXd(r, c);
	int i = 0;
	int j = 0;

	for (auto& it1 : _j.items()) {
		for (auto& it2 : it1.value().items()) {
			if (it2.value().is_string()) {
				_m(i, j) = this->categories[it2.value()];
			}
			else {
				_m(i, j) = it2.value();
			}
			
			cout << _m(i, j) << endl;
			j++;
		}
		i++;
		j = 0;
	}
}
void Trainer::randomizeDataSet() {

}

void Trainer::appendLayer(Layer _layer) {

}

void Trainer::initLayer(
	int* _nodeNumbers, VectorXd(*_activation)(VectorXd), VectorXd(*_derive)(VectorXd)
	) {
	
}

void Trainer::loadLayers(string _route) {
	std::ifstream i(_route);
	json l;
	i >> l;

	//
	
}

void Trainer::setLayer(int _idx, Layer _layer) {

}

void Trainer::setLossFunction(VectorXd(*_lossFunction)(VectorXd,VectorXd), VectorXd(*_lossDeriveFunction)(VectorXd,VectorXd)) {
	this->lossFunction = _lossFunction;
	this->lossDeriveFunction = _lossDeriveFunction;
}

MatrixXd Trainer::predict(int _dataset) {
	// return value:
		// x1 x2 ... xn y outputNodes
		// width = inputCount + outputCounts
		// height = number of data
	MatrixXd* mat;
	MatrixXd ret;
	int w, h;
	if (_dataset == TRAIN_DATA) {
		w = this->train_json[0].size() + this->hidden_layers.back().getNodeCount();
		h = this->train_json.size();
		mat = &(this->train_mat);
		
	}
	else if (_dataset == TEST_DATA) {
		w = this->test_json[0].size() + this->hidden_layers.back().getNodeCount();
		h = this->test_json.size();
		mat = &(this->test_mat);
	}
	else {
		mat = &MatrixXd(1, 1);
	}
	
	ret = MatrixXd(h, 2 * this->hidden_layers.back().getNodeCount());
	for (int i = 0; i < h; i++) {
		this->feedForward(mat->row(i).head(w - 2 * (this->hidden_layers.back().getNodeCount())));
		ret.row(i) << mat->row(i).tail(this->hidden_layers.back().getNodeCount()), this->hidden_layers.back().getOutput();
	}
	
	
	
	return ret;
}

void Trainer::feedForward(VectorXd _input) {
	//set input layer
	auto h = hidden_layers.begin();
	h->feedForward(_input);

	//feed to hidden layers
	for (h++; h != hidden_layers.end(); h++) {
		h->feedForward( (h - 1)->getOutput() );
	}
		
	//feed to output layer
	//hidden_layers.back().feedForward((h - 1)->getOutput());

}

void Trainer::calcLoss(VectorXd _input, VectorXd _target) {

	this->hidden_layers.back().calcLoss(
		_input,
		this->lossDeriveFunction(_target, this->hidden_layers.back().getOutput()) *	this->hidden_layers.back().getDerive()
	);
}

void Trainer::trainingSGD(int _iteration) {
	VectorXd tmp;
	int w = this->train_json[0].size() + this->hidden_layers.back().getNodeCount();
	int h = this->train_json.size();
	for (int i = 0; i < _iteration; i++) {
		//void Layer::backpropagation(VectorXd _input, VectorXd _derive, Layer* outputLayer) {
			//this->delta = outputLayer->w * outputLayer->delta * _derive;
			//w += learningRate * _input * this->delta;
		
		
		for (int i = 0; i < h; i++) {
			tmp = this->train_mat.row(i).head(w - 2 * (this->hidden_layers.back().getNodeCount()));
			this->feedForward(tmp);
			this->calcLoss (
				(this->hidden_layers.end() - 2)->getOutput(),
				this->train_mat.row(i).tail(this->hidden_layers.back().getNodeCount())
			);

			vector<Layer>::iterator it = this->hidden_layers.end();
			it--;
			it--;
			for (; it > this->hidden_layers.begin(); it--) {
				it->backpropagation((it - 1)->getOutput(), &(*(it + 1)));
			}
			it->backpropagation(tmp, &(*(it + 1)));
		}
	}
}


///////////////Spike Neural Network Implementation //////////////////
// node(w[in_node][synapse], net) - synapse (delays[])


