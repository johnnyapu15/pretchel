

#include "./../header/pretchel.h"
#include <iostream>

using namespace std;
using namespace Eigen;

int main() {
	MatrixXd w = MatrixXd::Zero(4, 2);
	w.Random(4,2);
	VectorXd b = VectorXd::Random(4);
	
	vector<int> neurals{ 4, 10, 12, 1};
	Trainer tr = Trainer(neurals, 0.005, tanh, tanh_derive, ReLU, ReLU_derive);
	//Trainer tr = Trainer(neurals, 0.005, ReLU, ReLU_derive);

	string folder = "C:/Users/Sw_engineer/Documents/JJA/proj/pretchel/data/iris/";
	//string folder = "C:/Users/hjan1/Documents/JJA/projects/pretchel/data/iris/";
	tr.setLossFunction(MSE, MSE_derive);
	vector<string> cat;
	cat.push_back("setosa");
	cat.push_back("versicolor");
	cat.push_back("virginica");

	tr.setInputdata(cat, folder + "train.json", folder + "test.json");
	MatrixXd ret = tr.predict(TRAIN_DATA);
	//cout << ret << endl;
	cout << MSE(ret.col(0), ret.col(1)).mean() << endl;

	for (int i = 0; i < 10; i++) {
		tr.trainingSGD(10);
		ret = tr.predict(TRAIN_DATA);
		//cout << ret << ret.mean() << endl;
		cout << "Iteration " << i << ": " << MSE(ret.col(0), ret.col(1)).mean() << endl;

	}

	return 0;
}