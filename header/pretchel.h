#pragma once

#ifndef PRETCHEL_H
#define PRETCHEL_H

#define TRAIN_DATA 0
#define TEST_DATA 1

#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <array>
#include "./../header/Eigen/Dense"
#include "./../header/json.hpp"


using namespace nlohmann;
using namespace std;
using namespace Eigen;




VectorXd tanh(VectorXd _input);
VectorXd tanh_derive(VectorXd _input);

VectorXd ReLU(VectorXd _input);
VectorXd ReLU_derive(VectorXd _input);

VectorXd softmaxLayer(VectorXd _input);

VectorXd MSE(VectorXd, VectorXd);
VectorXd MSE_derive(VectorXd, VectorXd);


class Node {
	//
};
class ActivationLayer {
private:
	VectorXd(*activation)(VectorXd);
	VectorXd(*derive)(VectorXd);
	VectorXd o;

public:
	ActivationLayer();
	ActivationLayer(VectorXd(*_activation)(VectorXd), VectorXd(*_derive)(VectorXd), int _node);
	void setFunction(VectorXd(*_activation)(VectorXd), VectorXd(*_derive)(VectorXd), int _node);
	void feedForward(VectorXd _input);
	VectorXd calcDerive(VectorXd _input);
	VectorXd getOutput();

};
class Layer {
	// CNN ���� �پ��� ���̾ ���� �����ϱ� ���ϰ� �������̽��� �����߰���
	// �ڱ� ��� ������ �����س���, ����ġ���� ��Ʈ��ũ �������� ���ϴ� �ɷ�.
private:
	double learningRate;
	int nodeCount;
	VectorXd net; // n
	VectorXd delta; // i * n
	MatrixXd w; // i * n
	VectorXd b; // n

public:
	ActivationLayer* actLayer = NULL;
	Layer();
	Layer(int _node, int _inputCount, double _learningRate);

	// Load from JSON file
	// json => vector => vector.data => VectorXf or MatrixXf
	Layer(int _node, json _jsonFile);

	int getNodeCount();
	void initLayer();

	void setActivation(VectorXd(*_activation)(VectorXd), VectorXd(*_derive)(VectorXd));

	void feedForward(VectorXd _input);

	void feedForward(VectorXd _input, VectorXd _output);

	void backpropagation(VectorXd _input, Layer * outputLayer);
	void calcLoss(VectorXd _input, MatrixXd _delta);
	VectorXd getOutput();
	VectorXd getDerive();
};

class NeuralNetwork {
	//���ο� ������ �͵�: ���̾��
	// �Լ�: forward, backward

};

class Trainer {
private:
	vector<Layer> hidden_layers;
	//Layer output_layer;
	json train_json;
	json test_json;
	MatrixXd train_mat;
	MatrixXd test_mat;
	map<string, int> categories;
	VectorXd (*lossFunction)(VectorXd, VectorXd);
	VectorXd(*lossDeriveFunction)(VectorXd, VectorXd);
	void feedForward(VectorXd);
	void calcLoss(VectorXd _input, VectorXd _target);
public:
	Trainer();
	Trainer(int * _nodeNumbers, VectorXd(*_activation)(VectorXd), VectorXd(*_derive)(VectorXd));
	Trainer(int _inputNumber, vector<int> _hiddenNodes, VectorXd(*_activation)(VectorXd), VectorXd(*_derive)(VectorXd));
	Trainer(int _inputNumber, vector<int> _hiddenNodes, double _learningRate, VectorXd(*_activation)(VectorXd), VectorXd(*_derive)(VectorXd), VectorXd(*_outputActivation)(VectorXd), VectorXd(*_outputDerive)(VectorXd));

	void setTrainData(string);
	void setTestData(string);
	void jsonToMat(json _j, MatrixXd & _m);
	void randomizeDataSet();
	void randomizeData();
	void appendLayer(Layer);
	void initLayer(int * _nodeNumbers, VectorXd(*_activation)(VectorXd), VectorXd(*_derive)(VectorXd));
	void loadLayers(string _route);
	void initLayer(int * _nodeNumbers);
	void setLayer(int _idx, Layer _layer);
	void setLossFunction(VectorXd(*_lossFunction)(VectorXd, VectorXd), VectorXd(*_lossDeriveFunction)(VectorXd, VectorXd));	MatrixXd predict(int _dataset);
	MatrixXd predict();
	void trainingSGD(int _iteration);
	void training(int _iteration);
	Trainer(vector<int> _hiddenNodes, double _learningRate, VectorXd(*_activation)(VectorXd), VectorXd(*_derive)(VectorXd));
	Trainer(vector<int> _hiddenNodes, double _learningRate, VectorXd(*_activation)(VectorXd), VectorXd(*_derive)(VectorXd), VectorXd(*_outputActivation)(VectorXd), VectorXd(*_outputDerive)(VectorXd));
	void setInputdata(vector<string>& _categories, string _trainRoute, string _testRoute);
};


///////////////Spike Neural Network Implementation //////////////////
// node(w[in_node][synapse], net) - synapse (delays[])

class SpikeNeuron {
private:
	MatrixXd w;
	double net;
public:

};

//More try more intense
class Synapse {
private:
	VectorXd delay;

public:

};



#endif